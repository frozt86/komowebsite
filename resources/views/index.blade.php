<!DOCTYPE html>
<html lang="{{ str_replace("_", "-", app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- TITLE --}}
    <title>Index</title>

    <meta name="description"
        content="Embark on a thrilling Komodo Daily Trip for unmatched adventures and travel experiences. Expert-guided tours, wildlife encounters, and unforgettable moments await. Book your trip now and discover the wonders of Komodo!">
    <meta name="robots" content="index, follow" />

    {{-- VITE --}}
    @vite(["resources/css/app.css", "resources/js/app.js"])

    {{-- LIVE-WIRE --}}
    <livewire:styles />
</head>

<body class="text-stone-700 bg-stone-50">
    This is Index page
</body>

</html>
