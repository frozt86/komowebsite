<!DOCTYPE html>
<html lang="{{ str_replace("_", "-", app()->getLocale()) }}" class="scroll-smooth">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- TITLE --}}
    <title>Komodo Full Day Trip</title>

    <meta name="description"
        content="Embark on a thrilling Komodo Daily Trip for unmatched adventures and travel experiences. Expert-guided tours, wildlife encounters, and unforgettable moments await. Book your trip now and discover the wonders of Komodo!">
    <meta name="robots" content="index, follow" />

    {{-- VITE --}}
    @vite(["resources/css/app.css", "resources/js/app.js"])

    {{-- LIVE-WIRE --}}
    <livewire:styles />
</head>

<body class="text-stone-700 bg-stone-50">
    {{-- LIVE-WIRE --}}
    <livewire:scripts />

    <a href="#top"
        class="w-[3em] h-[3em] flex items-center justify-center fixed bottom-4 right-4 sm:bottom-8 sm:right-8 rounded-full bg-violet-800 z-[11]">
        <i class="fa-solid fa-chevron-up text-stone-100"></i>
    </a>

    <nav id="top" class="absolute top-0 w-full text-stone-900 text-lg md:text-xl z-[10]">
        <ul class="py-8 px-6 flex items-center justify-center">
            <li class="mx-5 mix-blend-multiply">
                <a href="/" wire:navigate>
                    Komodo YouTrip
                </a>
            </li>
            <li class="grow sm:block hidden"></li>
            <li class="mx-5 mix-blend-multiply sm:block hidden">
                <a href="/" wire:navigate>Home</a>
            </li>
            <li class="mx-5 mix-blend-multiply sm:block hidden">
                <a href="privatetrip" wire:navigate>Private Trip</a>
            </li>
            <li class="mx-5 mix-blend-multiply sm:block hidden">
                <a href="about-us" wire:navigate>About Us</a>
            </li>
            <li class="mx-3 md:mx-5  sm:block hidden">
                <a href="/book" wire:navigate
                    class="px-5 py-2 bg-violet-700 rounded-full text-stone-50 font-medium text-center ">Book Now</a>
            </li>
        </ul>
    </nav>

    {{ $slot }}

    {{-- FOOTER SECTION --}}
    <footer class=" ">
        <div class="flex items-center justify-center bg-stone-700 text-stone-50 p-20">
            <div>
                <div>Affiliate Websites</div>
                <ul>
                    <li>Ascend</li>
                    <li>Ascend</li>
                    <li>Ascend</li>
                    <li>Ascend</li>
                </ul>
            </div>
            <div>
                <div>Site</div>
            </div>
            <div>
                <div>Social Media</div>
            </div>
            <div>
                <div>Contact Us</div>
            </div>
        </div>
        <div class="bg-stone-800 text-stone-50 p-10 text-center">
            Copyright © Komodoyoutrip
        </div>
    </footer>
</body>

</html>
