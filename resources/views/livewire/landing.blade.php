<div>

    <button
        class="w-[3em] h-[3em] flex items-center justify-center fixed bottom-4 right-4 sm:bottom-8 sm:right-8 rounded-full bg-violet-800 z-[10]">
        <i class="fa-solid fa-chevron-up text-stone-100"></i>
    </button>

    {{-- HERO SECTION --}}
    <div id="hero" class="box-border bg-origin-border bg-cover bg-center pt-[3rem]"
        style="background-image: url('{{ Vite::asset("resources/medias/speedboat-running-rect.jpeg") }}');">

        <div class="relative h-[60rem]">
            <div class="absolute top-[7rem] sm:top-[6rem] w-full m-auto mix-blend-multiply">
                <h1 class="sm:text-8xl text-6xl text-center text-stone-700/90 font-medium ">
                    6 Fantastic Places<br />1 Day Trip
                </h1>
                <p class="mt-14 text-center font-thin text-3xl">
                    Komodo's Wonders Unforgettable Trip!
                </p>
            </div>
            <div class="absolute bottom-[5rem] sm:bottom-[4.5rem] w-full text-center text-stone-50">
                <a href="/book"
                    class="block w-[28rem] mx-auto px-5 py-2 text-stone-300 font-medium text-5xl text-center ">
                    <span class="">
                        Book Now
                    </span>
                    <i class="fa-solid fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>

    {{-- BOOK SECTION --}}
    @php
        $tomorrow = now()->addDay();
        $overmorrow = now()->addDays(2);
    @endphp
    <div id="schedule" class="py-24 px-20 bg-stone-100">
        <div class="my-[3rem]">
            <h2 class="text-center text-7xl font-medium text-stone-700 ">
                <span class="">Embark on a voyage with us</span>
            </h2>
        </div>
        <div class="py-[2rem]">
            <div class="p-5 m-5">
                <button
                    class="w-full flex justify-center items-center box-border border-2 bg-violet-800 text-stone-50 rounded-full px-20 p-10 h-[14rem] ">
                    <div class="w-[23rem] text-left">
                        <div class="text-4xl font-medium">
                            <span class="text-5xl text-lime-300">Tomorrow</span>
                        </div>
                        <div class="text-4xl text-left ml-3">
                            <span class="">{{ $tomorrow->shortEnglishMonth }}</span>
                            <span class="font-medium">{{ $tomorrow->day }}</span>,
                            <span class="text-2xl">{{ $tomorrow->year }}</span>
                        </div>
                    </div>
                    <div class="w-[1px] h-[10rem] mx-10 bg-stone-100">

                    </div>
                    <div class="grow text-left">
                        <span class="text-3xl line-through italic">IDR 1.400.000</span>
                        <span class="text-5xl ml-10 text-amber-300">IDR 1.300.000</span>
                    </div>
                    <div class="ml-10 text-7xl">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </button>
            </div>
            <div class="p-5 m-5">
                <button
                    class="w-full flex justify-center items-center box-border bg-violet-800 text-stone-50 rounded-full px-20 p-10 h-[14rem]">
                    <div class="w-[23rem] text-left">
                        <div class="text-5xl font-medium">
                            <span class="text-5xl text-green-300">Overmorrow</span>
                        </div>
                        <div class="text-4xl text-left ml-3">
                            <span class="">{{ $overmorrow->shortEnglishMonth }}</span>
                            <span class="font-medium">{{ $overmorrow->day }}</span>,
                            <span class="text-2xl">{{ $overmorrow->year }}</span>
                        </div>
                    </div>
                    <div class="w-[1px] h-[10rem] mx-10 bg-stone-100">

                    </div>
                    <div class="grow text-left">
                        <span class="text-3xl line-through italic">IDR 1.400.000</span>
                        <span class="text-5xl ml-10 text-amber-300">IDR 1.300.000</span>
                    </div>
                    <div class="ml-10 text-7xl">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </button>
            </div>
            <div class="p-5 m-5">
                <button
                    class="w-full flex justify-center items-center box-border bg-violet-800 text-stone-50 rounded-full px-20 p-10 h-[14rem] ">
                    <div class="grow">
                        <div class="text-center w-full text-5xl">
                            <i class="fa-solid fa-calendar-days"></i> Pick Your Date
                        </div>
                    </div>
                    <div class="ml-10 text-7xl">
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                </button>
            </div>
        </div>
    </div>

    {{-- ACTIVITY SECTION --}}
    <div id="activities" class="py-24  ">
        <div class="my-[5rem]">
            <h2 class="text-center text-7xl font-medium text-stone-700 ">
                Activities
            </h2>
            <p class="text-center mt-5 text-xl">
                Adventure Awaits: Exciting Activities for Every Explorer
            </p>
        </div>
        <div class="relative mb-[5rem]">
            {{-- GARIS --}}
            <div class="absolute border border-violet-200 h-full left-1/2 z-[-1]"></div>

            {{-- Start --}}
            <div class="flex justify-center mt-32">
                <div class="w-5/12 p-10 flex justify-start items-center">
                </div>
                <div class="flex justify-center items-center">
                    <div
                        class="rounded-full border-[5px] border-violet-400 bg-stone-50 w-48 h-48 flex justify-center items-center ">
                        <div class="text-center">
                            <span class="text-3xl text-stone-500 font-medium">
                                Start
                            </span>
                            <br />
                            <span class="">
                                07:00AM<a href="#conditions" class="font-bold text-violet-700">*</a>
                            </span>
                        </div>

                    </div>
                </div>
                <div class="w-5/12 p-10">
                </div>

            </div>

            {{-- First Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 flex justify-end items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/puncak-padar-square.jpeg") }}');">

                    </div>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 p-10">
                    <h3 class="text-4xl font-medium">
                        <span class='font-bold text-5xl'>Padar Island </span> Trekking Adventure
                    </h3>
                    <p class="mt-10 text-justify pr-20 text-xl">
                        Kickstart your expedition with a thrilling trek on Padar Island, an elevated paradise
                        boasting
                        three pristine bays. Ascend to the summit for panoramic views that showcase the
                        distinctively
                        shaped beaches and the vast expanse of the Komodo archipelago. Capture the beauty of sunrise
                        or
                        sunset for an unforgettable experience etched in your memory.
                    </p>
                </div>
            </div>

            {{-- Second Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 p-10 text-right">
                    <h3 class="text-4xl font-medium">
                        Serenity at <span class='font-bold text-5xl'>Pink Beach </span>
                    </h3>
                    <p class="mt-10 text-justify pl-20 text-xl">
                        Revel in the tranquility of Pink Beach, renowned for its unique pink-hued sands. Unlike
                        traditional snorkeling activities, here you'll enjoy a peaceful beach experience. Stroll
                        along
                        the shoreline, feel the soft grains of pink sand beneath your feet, and bask in the
                        picturesque
                        surroundings. Pink Beach offers a serene atmosphere for relaxation and appreciation of the
                        natural beauty.
                    </p>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 flex justify-start items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/pink-beach-tanning-square.jpeg") }}');">

                    </div>
                </div>
            </div>

            {{-- Third Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 flex justify-end items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/komodo-walking-square.jpeg") }}');">

                    </div>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 p-10">
                    <h3 class="text-4xl font-medium">
                        Encounter the Legendary <span class='font-bold text-5xl'>Komodo Dragons</span>
                    </h3>
                    <p class="mt-10 text-justify pr-20 text-xl">
                        Immerse yourself in the raw wilderness of Komodo Island, the only habitat of the iconic
                        Komodo
                        dragons. Accompanied by knowledgeable guides, witness these prehistoric creatures up close
                        and
                        gain insights into their behavior, natural habitat, and the conservation efforts in place to
                        protect them. It's a unique opportunity to connect with these fascinating reptiles.
                    </p>
                </div>
            </div>

            {{-- Fourth Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 p-10 text-right">
                    <h3 class="text-4xl font-medium">
                        Unwind at <span class='font-bold text-5xl'>Taka Makasar</span>'s Sandbar
                    </h3>
                    <p class="mt-10 text-justify pl-20 text-xl">
                        Escape to the secluded Taka Makasar, a pristine sandbar surrounded by turquoise waters.
                        Here,
                        you can unwind and relax, whether it's sunbathing, taking a refreshing dip in the shallow
                        waters, or simply appreciating the untouched beauty of this natural oasis. Taka Makasar
                        offers a
                        perfect setting for rejuvenation
                    </p>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 flex justify-start items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/taka-makasar-person-square.jpeg") }}');">

                    </div>
                </div>
            </div>

            {{-- Fifth Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 flex justify-end items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/manta-single-square.jpeg") }}');">

                    </div>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 p-10">
                    <h3 class="text-4xl font-medium">
                        Dive into the Depths of <span class='font-bold text-5xl'>Manta Point</span>
                    </h3>
                    <p class="mt-10 text-justify pr-20 text-xl">
                        For thrill-seekers, Manta Point provides an exhilarating experience. Plunge into the
                        crystal-clear waters and witness the majestic manta rays gracefully gliding through the
                        depths.
                        This underwater spectacle is sure to leave you in awe, making it a must-visit site for
                        diving
                        enthusiasts.
                    </p>
                </div>
            </div>

            {{-- Sixth Activity --}}
            <div class="flex justify-center items-center mt-24">
                <div class="w-5/12 p-10 text-right">
                    <h3 class="text-4xl font-medium">
                        <span class='font-bold text-5xl'>Kenawa Island</span> Snorkeling Extravaganza
                    </h3>
                    <p class="mt-10 text-justify pl-20 text-xl">
                        Conclude your expedition with an enchanting snorkeling adventure around Kenawa Island.
                        Navigate
                        the waters near the bridge, where you'll encounter a plethora of colorful fishes. Expert
                        guides
                        will accompany you, enhancing your experience by pointing out various marine species.
                        Immerse
                        yourself in the vibrant underwater world, creating lasting memories in this captivating
                        environment.
                    </p>
                </div>
                <div class="w-2/12 flex justify-center items-center">
                    <div class="bg-violet-500 text-stone-50 w-4 h-4 p-3 rounded-full">
                    </div>
                </div>
                <div class="w-5/12 flex justify-start items-center">
                    <div class="rounded-full overflow-hidden border-[5px] border-violet-400 w-[28rem] h-[28rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                        style="background-image: url('{{ Vite::asset("resources/medias/kid-snorkeling-square.jpeg") }}');">

                    </div>
                </div>
            </div>

            {{-- End --}}
            <div class="flex justify-center mt-32">
                <div class="w-5/12 p-10 flex justify-start items-center">
                </div>
                <div class="flex justify-center items-center">
                    <div
                        class="rounded-full border-[5px] border-violet-400 bg-stone-50 w-40 h-40 flex justify-center items-center ">
                        <div class="text-center">
                            <span class="text-3xl text-stone-500 font-medium">
                                Finish
                            </span>
                            <br />
                            <span class="">
                                05:00PM<a href="#conditions" class="font-bold text-violet-700">*</a>
                            </span>
                        </div>

                    </div>
                </div>
                <div class="w-5/12 p-10">
                </div>
            </div>

        </div>
    </div>

    {{-- GUIDELINES SECTION --}}
    <div id="guidelines" class="py-24 px-20 bg-stone-100 ">
        <div class="my-[5rem]">
            <h2 class="text-center text-7xl font-medium ">
                <span class="">Guidelines</span>
            </h2>
            <p class="text-center mt-5 text-xl">
                Navigating Excellence: Essential Guidelines for Your Journey
            </p>
        </div>

        <div class="mt-32 mb-[5rem] grid grid-cols-3 gap-20">
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/world-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Respect Nature and Preserve Ecosystems
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Refrain from collecting souvenirs such as corals, shells, or sands.
                        </li>
                        <li>
                            Avoid touching or disturbing flora and fauna; maintain a safe distance.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/recycle-bin-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Maintain Cleanliness and No Smoking
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Dispose of waste properly in designated bins; carry a small trash bag for personal items
                        </li>
                        <li>
                            Refrain from smoking to prevent fire hazards and respect the park's clean air
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/vest-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Ensure Safety, Especially During Snorkeling
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Wear appropriate safety gear during water activities, including a life jacket.
                        </li>
                        <li>
                            Be mindful of your surroundings, currents, and other snorkelers
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/compliant-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Adhere to Local Rules and Guide Instructions
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Respect and follow guidelines provided by local authorities and guides
                        </li>
                        <li>
                            Comply with rules set forth by Komodo National Park to protect the environment
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/sun-protection-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Weather-Appropriate Preparations
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Bring sun protection such as sunscreen, sunglasses, and a hat during sunny days
                        </li>
                        <li>
                            Carry an umbrella or raincoat in case of unexpected rain
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/sport-wear-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Dress Appropriately
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            Wear suitable clothing for outdoor activities, considering weather conditions
                        </li>
                        <li>
                            Choose lightweight and breathable fabrics for comfort during hikes and treks
                        </li>
                    </ul>
                </div>
            </div>
            <div></div>
            <div class="">
                <div class="text-center w-20 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/drone-freepik.png") }}" />
                </div>
                <h3 class="py-5 px-10 text-center text-3xl font-bold">
                    Drone Usage Protocol
                </h3>
                <div class="py-5 px-10 text-justify text-lg">
                    <ul class="list-disc">
                        <li>
                            If you plan to use a drone, inform park authorities or relevant personnel in advance
                        </li>
                        <li>
                            Follow local regulations and guidelines for responsible drone usage
                        </li>
                    </ul>
                </div>
            </div>
            <div></div>
        </div>


    </div>

    {{-- INCLUSION SECTION --}}
    <div id="inclusions" class="py-24 px-20 ">
        <div class="grid grid-cols-2 gap-20 my-[2rem]">
            <div class="p-10">
                <div class="text-center w-28 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/hand-checked-smashicons.png") }}" />
                </div>
                <h2 class="text-center text-5xl font-medium mt-10">
                    <span class=" ">Inclusive Adventure Package</span>
                </h2>
                <ol class="list-decimal mt-24 text-2xl">
                    <li class="mb-5">
                        <h4 class="font-medium">Shuttle Hotel Transport: <i class="fa-solid fa-van-shuttle"></i></h4>
                        <p class="text-lg text-justify">
                            Hassle-free hotel transportation included for a convenient start to your adventure.
                            Enjoy a comfortable shuttle service ensuring a smooth journey to and from your
                            accommodation.
                        </p>
                    </li>
                    <li class="mb-5">
                        <h4 class="font-medium">Snorkeling Gear (Mask and Fins) + Safety Vest: <i
                                class="fa-solid fa-mask"></i></h4>
                        <p class="text-lg text-justify">
                            Dive into the underwater wonders with complimentary high-quality snorkeling gear.
                            Ensure safety and ease during your snorkeling experience with provided safety vests.
                        </p>
                    </li>
                    <li class="mb-5">
                        <h4 class="font-medium">Lunch, Snack, and Water: <i class="fa-solid fa-drumstick-bite"></i> <i
                                class="fa-solid fa-bottle-water"></i>
                        </h4>
                        <p class="text-lg text-justify">
                            Indulge in a delicious and inclusive meal, including lunch and a satisfying snack.
                            Stay refreshed throughout your adventure with complimentary water to keep you energized.
                        </p>
                    </li>
                    <li class="mb-5">
                        <h4 class="font-medium">Travel Insurance: <i class="fa-solid fa-file-circle-check"></i></h4>
                        <p class="text-lg text-justify">
                            Travel with peace of mind as your adventure is covered by comprehensive travel insurance.
                            Enjoy added security and protection for unforeseen circumstances during your exploration.
                        </p>
                    </li>
                </ol>
            </div>

            <div class="p-10">
                <div class="text-center w-28 mx-auto">
                    <img src="{{ Vite::asset("resources/icons/hand-exclude-freepik.png") }}" />
                </div>
                <h2 class="text-center text-5xl font-medium mt-10">
                    <span class=" ">Exclusive Adventure Experience</span>
                </h2>
                <ol class="list-decimal mt-24 text-2xl">
                    <li class="mb-5">
                        <h4 class="font-medium">National Park Entrance Fee: <i class="fa-solid fa-ticket"></i></h4>
                        <p class="text-lg text-justify">
                            Please note that the National Park entrance fee is not included.
                            Be prepared to cover the entrance fee separately for an immersive experience in the
                            protected natural wonders.
                            <br>
                            Note: Bring cash
                        </p>
                    </li>
                    <li class="mb-5">
                        <h4 class="font-medium">Personal Expenses: <i class="fa-solid fa-money-bill-wave"></i></h4>
                        <p class="text-lg text-justify">
                            While we provide an enriching adventure, personal expenses are not included.
                            Plan accordingly for any additional purchases or optional activities during your
                            exploration.
                        </p>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    {{-- CLOSING SECTION --}}
    <div id="closing" class="py-24 px-20 bg-stone-100">
        <div class="py-[5rem] px-48 text-5xl text-center text-stone-700 mix-blend-luminosity ">
            <div class="font-medium">Ready for an unforgettable journey?</div>
            <div class="text-4xl mt-10">
                Book with us and let the adventure of a lifetime unfold.
                <br>
                Secure your spot now for seamless experiences and cherished memories
            </div>
        </div>
        <div class="py-[5rem] flex justify-center">
            <div class="rounded-full overflow-hidden w-[50rem] h-[50rem] flex justify-center items-center bg-cover bg-no-repeat bg-center"
                style="background-image: url('{{ Vite::asset("resources/medias/padar-trekking-square.jpeg") }}');">
            </div>
        </div>
        <div class="py-[5rem] flex justify-center">
            <a href="/book" wire:navigate class="p-10 text-5xl bg-violet-700 text-stone-50 rounded-full">
                Book Now <i class="fa-solid fa-arrow-right"></i>
            </a>
        </div>
    </div>



    {{-- CONDITION SECTION --}}
    <div id="conditions" class="p-10 flex justify-center items-center text-sm bg-stone-200">
        * : Please be advised that the activities mentioned in this itinerary are scheduled in accordance with
        UTC+08:00 local time. However, the timing of these activities may be subject to change based on prevailing
        circumstances. Factors such as weather conditions, operational considerations, and other unforeseen
        situations may lead to variations in the scheduled timings. Your understanding and flexibility are
        appreciated as we aim to provide the best possible experience while prioritizing safety and optimal
        conditions for each activity. Thank you for your cooperation.
    </div>
</div>
