<div class="box-border p-2 w-full">
    @php
        $loop = true;
        $days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        $dates = [];

        $today = now();
        $thisMonthCal = clone $displayCal;
        $lastMonthCal = clone $thisMonthCal;
        $nextMonthCal = clone $thisMonthCal;

        $lastMonthCal->month -= 1;
        $lastMonthCal->lastOfMonth();
        $lastMonthDay = $lastMonthCal->dayOfWeek;
        $lastMonthCal->day -= $lastMonthDay - 1;
        for ($i = 1; $i <= $lastMonthDay; $i++) {
            array_push($dates, [
                "date" => $lastMonthCal->day,
                "isCurrentMonth" => false,
                "value" => clone $lastMonthCal,
            ]);
            $lastMonthCal->day++;
        }

        $thisMonthCal->day = 1;
        for ($i = 1; $i <= $thisMonthCal->daysInMonth; $i++) {
            array_push($dates, [
                "date" => $thisMonthCal->day,
                "isCurrentMonth" => true,
                "value" => clone $thisMonthCal,
            ]);
            if ($i !== $thisMonthCal->daysInMonth) {
                $thisMonthCal->day++;
            }
        }
        $nextMonthCal->month += 1;
        $nextMonthCal->day = 1;
        $nextMonthDay = $nextMonthCal->dayOfWeekIso;
        if ($nextMonthCal->dayOfWeek !== 1) {
            for ($i = 1; $i <= 8 - $nextMonthDay; $i++) {
                array_push($dates, [
                    "date" => $nextMonthCal->day,
                    "isCurrentMonth" => false,
                    "value" => clone $nextMonthCal,
                ]);
                $nextMonthCal->day++;
            }
        }

    @endphp
    <div class="flex p-1 items-center">
        <button wire:click="showPrevMonth" class="p-2">
            <i class="fa-solid fa-arrow-left text-lg text-indigo-700"></i>
        </button>
        <button wire:click="displayToday" class="p-2 mx-2">
            <i class="fa-solid fa-calendar-day text-lg text-indigo-700"></i>
        </button>
        <div class="text-2xl text-center grow text-stone-700">
            {{ $thisMonthCal->englishMonth . ", " . $thisMonthCal->year }}
        </div>
        <button wire:click="displaySelected" class="p-2 mx-2">
            <i class="fa-solid fa-calendar-check text-lg text-indigo-700"></i>
        </button>
        <button wire:click="showNextMonth" class="p-2">
            <i class="fa-solid fa-arrow-right text-lg text-indigo-700"></i>
        </button>
    </div>
    <hr class="h-px bg-stone-400 border-0 my-2" />
    <div class="grid grid-cols-7 mb-1 ">
        @foreach ($days as $i => $o)
            <div class="m-1 font-bold text-sm text-center text-stone-700">
                {{ $o }}
            </div>
        @endforeach
    </div>
    <div class="grid grid-cols-7">
        @foreach ($dates as $i => $o)
            @php
                $isSelected = false;
                foreach ($values as $y => $u) {
                    if ($u->toDateString() === $o["value"]->toDateString()) {
                        $isSelected = true;
                        break;
                    }
                }
                $isToday = $today->toDateString() === $o["value"]->toDateString();
            @endphp
            <button wire:click="selectDate('{{ $o["value"]->toDateString() }}')"
                class="text-center p-2 {{ $isToday ? "text-amber-600 font-bold" : ($isSelected ? "text-stone-50" : ($o["isCurrentMonth"] ? "text-stone-700" : "text-stone-300")) }} {{ $isSelected ? "bg-indigo-600 rounded-xl" : null }}">
                {{ $o["date"] }}
            </button>
        @endforeach

    </div>
    {{-- <div class="p-2">
        <label class="relative inline-flex items-center mr-5 cursor-pointer" wire:click="toggleMultiple">
            <input type="checkbox" wire:model.defer="multipleMode" class="sr-only peer">
            <div
                class="w-11 h-6 bg-stone-200 rounded-full peer peer-focus:ring-4 peer-focus:ring-indigo-300 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-stone-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-indigo-600">
            </div>
            <span
                class="ml-3 text-sm font-medium text-stone-900 dark:text-stone-300 {{ $multipleMode ? "font-bold" : "font-normal" }}">
                Multiple
            </span>
        </label>
    </div> --}}
</div>
