<div>
    <div class="mt-[10rem] ">
        <h1 class="text-center text-7xl font-medium text-stone-700 ">
            <span class="">One Single Step Closer</span>
        </h1>
        <p class="text-center mt-5 text-xl">
            Elevating Journeys, Redefining Experiences
        </p>
    </div>

    <div class="pt-[6rem] ml-[8rem] text-5xl font-medium">
        <div class="inline-block p-10 rounded-t-3xl bg-violet-700 text-stone-50">
            Book Form
        </div>
    </div>

    <div class="py-[3rem] mb-[3rem] px-[2rem] mx-[3rem] flex items-start rounded-3xl border-4 border-violet-700">
        {{-- SUMMARY --}}
        <div class="sticky top-5 w-[20rem]">
            {{ $value }}
        </div>

        <div class="grow ml-10">
            {{-- DATEPICKER --}}
            <div class="box-border p-5 bg-stone-100 rounded-xl xl:w-[42rem] mx-auto">
                @php
                    $loop = true;
                    $days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
                    $dates = [];

                    $today = now();
                    $thisMonthCal = clone $displayCal;
                    $lastMonthCal = clone $thisMonthCal;
                    $nextMonthCal = clone $thisMonthCal;

                    $lastMonthCal->month -= 1;
                    $lastMonthCal->lastOfMonth();
                    $lastMonthDay = $lastMonthCal->dayOfWeek;
                    $lastMonthCal->day -= $lastMonthDay - 1;
                    for ($i = 1; $i <= $lastMonthDay; $i++) {
                        array_push($dates, [
                            "date" => $lastMonthCal->day,
                            "isCurrentMonth" => false,
                            "value" => clone $lastMonthCal,
                        ]);
                        $lastMonthCal->day++;
                    }

                    $thisMonthCal->day = 1;
                    for ($i = 1; $i <= $thisMonthCal->daysInMonth; $i++) {
                        array_push($dates, [
                            "date" => $thisMonthCal->day,
                            "isCurrentMonth" => true,
                            "value" => clone $thisMonthCal,
                        ]);
                        if ($i !== $thisMonthCal->daysInMonth) {
                            $thisMonthCal->day++;
                        }
                    }
                    $nextMonthCal->month += 1;
                    $nextMonthCal->day = 1;
                    $nextMonthDay = $nextMonthCal->dayOfWeekIso;
                    if ($nextMonthCal->dayOfWeek !== 1) {
                        for ($i = 1; $i <= 8 - $nextMonthDay; $i++) {
                            array_push($dates, [
                                "date" => $nextMonthCal->day,
                                "isCurrentMonth" => false,
                                "value" => clone $nextMonthCal,
                            ]);
                            $nextMonthCal->day++;
                        }
                    }

                @endphp
                {{-- MONTH DISPLAY --}}
                <div class="flex p-2 items-center justify-center">
                    {{-- PREV MONTH BUTTON --}}
                    <div>
                        <button wire:click="showPrevMonth" class="p-3">
                            <i class="fa-solid fa-arrow-left text-3xl text-violet-700"></i>
                        </button>
                    </div>
                    <div class="text-5xl text-center text-stone-700 grow">
                        {{ $thisMonthCal->englishMonth . ", " . $thisMonthCal->year }}
                    </div>
                    {{-- NEXT MONTH BUTTON --}}
                    <div>
                        <button wire:click="showNextMonth" class="p-3">
                            <i class="fa-solid fa-arrow-right text-3xl text-violet-700"></i>
                        </button>
                    </div>
                </div>
                {{-- LINE --}}
                <hr class="h-px bg-stone-200 border-0 my-3" />

                {{-- CALENDAR DISPLAY --}}
                <div class="flex items-center">
                    <div class="grow">
                        <div class="grid grid-cols-7 mb-5">
                            @foreach ($days as $i => $o)
                                <div
                                    class="m-1 font-medium text-3xl text-center {{ $i === 6 ? "text-rose-600" : "text-stone-700" }}">
                                    {{ $o }}
                                </div>
                            @endforeach
                        </div>
                        <div class="grid grid-cols-7">
                            @foreach ($dates as $i => $o)
                                @php
                                    $isSelected = false;
                                    if ($value) {
                                        if ($value->toDateString() === $o["value"]->toDateString()) {
                                            $isSelected = true;
                                        }
                                    }
                                @endphp
                                <div wire:click="selectDate('{{ $o["value"]->toDateString() }}')"
                                    class="p-1 flex justify-center ">
                                    <div>
                                        <button
                                            class="w-12 h-12 text-xl text-center {{ $isSelected ? "text-stone-50" : ($o["isCurrentMonth"] ? "text-stone-700" : "text-stone-300") }} {{ $isSelected ? "bg-violet-600 rounded-xl" : null }}">
                                            {{ $o["date"] }}
                                        </button>
                                    </div>

                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
                {{-- CALNEDAR NAV --}}
                <div class="flex p-10 items-center justify-center">
                    <button wire:click="displayToday" class="p-2 mx-2 text-violet-700 text-2xl">
                        <i class="fa-solid fa-calendar-day"></i> <span class="font-medium">This Month</span>
                    </button>
                    <button wire:click="displaySelected" class="p-2 mx-2 text-violet-700 text-2xl">
                        <i class="fa-solid fa-calendar-check "></i> <span
                            class="font-medium">{{ $value ? $value->toFormattedDateString() : "Selected Date" }}</span>
                    </button>
                </div>
            </div>
            {{-- FORM --}}
            <div class="box-border mt-[2rem] p-5 bg-stone-100 rounded-xl">
                ini form
            </div>
        </div>
    </div>
</div>
