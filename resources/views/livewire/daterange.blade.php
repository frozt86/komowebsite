<div class="box-border p-2 w-full">
    @php
        $loop = true;
        $days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        $dates = [];

        $today = now();
        $thisMonthCal = clone $displayCal;
        $lastMonthCal = clone $thisMonthCal;
        $nextMonthCal = clone $thisMonthCal;

        $lastMonthCal->month -= 1;
        $lastMonthCal->lastOfMonth();
        $lastMonthDay = $lastMonthCal->dayOfWeek;
        $lastMonthCal->day -= $lastMonthDay - 1;
        for ($i = 1; $i <= $lastMonthDay; $i++) {
            array_push($dates, [
                "date" => $lastMonthCal->day,
                "isCurrentMonth" => false,
                "value" => clone $lastMonthCal,
            ]);
            $lastMonthCal->day++;
        }

        $thisMonthCal->day = 1;
        for ($i = 1; $i <= $thisMonthCal->daysInMonth; $i++) {
            array_push($dates, [
                "date" => $thisMonthCal->day,
                "isCurrentMonth" => true,
                "value" => clone $thisMonthCal,
            ]);
            if ($i !== $thisMonthCal->daysInMonth) {
                $thisMonthCal->day++;
            }
        }
        $nextMonthCal->month += 1;
        $nextMonthCal->day = 1;
        $nextMonthDay = $nextMonthCal->dayOfWeekIso;
        if ($nextMonthCal->dayOfWeek !== 1) {
            for ($i = 1; $i <= 8 - $nextMonthDay; $i++) {
                array_push($dates, [
                    "date" => $nextMonthCal->day,
                    "isCurrentMonth" => false,
                    "value" => clone $nextMonthCal,
                ]);
                $nextMonthCal->day++;
            }
        }
    @endphp

    <div class="flex p-1 items-center">
        <button wire:click="showPrevMonth" class="p-2">
            <i class="fa-solid fa-arrow-left text-lg text-indigo-700"></i>
        </button>
        <button wire:click="displayToday" class="p-2 mx-2">
            <i class="fa-solid fa-calendar-day text-lg text-indigo-700"></i>
        </button>
        <div class="relative text-xl text-center grow text-stone-700">
            {{ $thisMonthCal->englishMonth . ", " . $thisMonthCal->year }}
            @if ($pickMode)
                <div
                    class="absolute bg-indigo-600/90 text-sm text-stone-50 rounded-b-lg p-2 top-0 left-0 right-0 z-[11]">
                    Pick : End Date
                </div>
            @endif
        </div>
        <div class="shrink">
            <button wire:click="displayStartValue" class="p-2 ">
                <i class="fa-solid fa-backward-fast text-lg text-indigo-700"></i>
            </button>
            <button wire:click="displayEndValue" class="p-2 ">
                <i class="fa-solid fa-forward-fast text-lg text-indigo-700"></i>
            </button>
        </div>
        <button wire:click="showNextMonth" class="p-2">
            <i class="fa-solid fa-arrow-right text-lg text-indigo-700"></i>
        </button>
    </div>
    <hr class="h-px bg-stone-400 border-0 my-2" />
    <div class="grid grid-cols-7 mb-1 ">
        @foreach ($days as $i => $o)
            <div class="m-1 font-bold text-sm text-center text-stone-700">
                {{ $o }}
            </div>
        @endforeach
    </div>
    <div class="grid grid-cols-7">
        @foreach ($dates as $i => $o)
            @php
                $isStart = $startValue ? $startValue->toDateString() === $o["value"]->toDateString() : false;
                $isEnd = !$pickMode && $endValue ? $endValue->toDateString() === $o["value"]->toDateString() : false;
                $isMiddle = !$pickMode && $endValue ? $o["value"]->greaterThan($startValue) && $o["value"]->lessThan($endValue) : false;
            @endphp
            <button wire:click="selectDate('{{ $o["value"]->toDateString() }}')"
                class="text-center p-2 {{ $today->toDateString() === $o["value"]->toDateString() ? "text-amber-600 font-bold" : ($o["isCurrentMonth"] ? ($isStart || $isEnd || $isMiddle ? "text-stone-50" : "text-stone-700") : "text-stone-300") }} {{ $isStart ? "bg-indigo-600 rounded-l-full" : null }} {{ $isEnd ? "bg-indigo-600 rounded-r-full" : null }} {{ $isMiddle ? "bg-indigo-600" : null }}">
                {{ $o["date"] }}
            </button>
        @endforeach

    </div>
</div>
