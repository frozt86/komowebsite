<?php

namespace App\Livewire;

use DateTime;
use Livewire\Component;
use Livewire\Attributes\On;
use Illuminate\Support\Carbon;

class Book extends Component
{

    public $value = null;
    public $min = null;
    public $displayCal = null;
    // ***************
    // LIFECYCLE HOOKS
    // ***************
    public function mount($value = null)
    {
        if ($value) {
            $cValue = Carbon::createFromFormat("Y-m-d H:i:s", $value)->setTime(0, 0, 0, 0);
            $this->value = $cValue;
        }

        $this->displayCal = $value ? $value : now();
        $this->displayCal->day = 1;

        $this->min = now()->setTime(0, 0, 0, 0);
    }

    public function render()
    {
        return view('livewire.book');
    }

    // **************
    // EVENT LISTENER
    // **************

    // ***************
    // OTHER FUNCTIONS
    // ***************
    public function selectDate($date)
    {
        $sDate = Carbon::createFromFormat("Y-m-d H:i:s", "$date 00:00:00");
        $this->value = $sDate;
        $this->displayCal = clone $sDate;
        $this->displayCal->day = 1;
        $this->dispatch('date-selected', value: $this->value);
    }

    public function displayToday()
    {
        $this->displayCal = now();
        $this->displayCal->day = 1;
    }

    public function displaySelected()
    {
        $this->displayCal = clone ($this->value ? $this->value : now());
        $this->displayCal->day = 1;
    }

    public function showNextMonth()
    {
        $this->displayCal->addMonth(1);
        $this->displayCal->day = 1;
    }

    public function showPrevMonth()
    {
        $this->displayCal->subMonth(1);
        $this->displayCal->day = 1;
    }
}
