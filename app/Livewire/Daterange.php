<?php

namespace App\Livewire;

use DateTime;
use Livewire\Component;
use Livewire\Attributes\On;
use Illuminate\Support\Carbon;

class Daterange extends Component
{
    public $startValue = null;
    public $endValue = null;
    public $min = null;
    public $max = null;
    public $displayCal = null;
    public $parentComponent = null;
    public $id = null;

    public $pickMode = false;

    // ***************
    // LIFECYCLE HOOKS
    // ***************
    public function mount(string $max = null, string $min = null, $id = null)
    {
        $this->displayCal = now();
        $this->displayCal->day = 1;
        $this->id = $id;
        if ($max) {
            $this->max = DateTime::createFromFormat("Y-m-d", $max);
        }
        if ($min) {
            $this->min = DateTime::createFromFormat("Y-m-d", $min);
        }
    }

    public function render()
    {
        return view('livewire.daterange');
    }

    // **************
    // EVENT LISTENER
    // **************
    #[On('refresh')]
    public function refresh()
    {
        $this->reset(['pickMode']);
        $this->startValue = now();
        $this->endValue = now();
        $this->endValue->day += 1;
    }

    // ***************
    // OTHER FUNCTIONS
    // ***************
    public function selectDate($date)
    {
        $sDate = Carbon::createFromFormat("Y-m-d H:i:s", "$date 00:00:00");
        if (!$this->pickMode) {
            $this->startValue = $sDate;
            $this->endValue = null;
            $this->pickMode = true;
            $this->displayCal = clone $this->startValue;
        } else {
            if (!$sDate->lessThanOrEqualTo($this->startValue)) {
                $this->endValue = $sDate;
                $this->pickMode = false;
                $this->dispatch('date-range-selected', id: $this->id, values: [$this->startValue, $this->endValue]);
                $this->displayCal = clone $this->endValue;
            }
        }
        $this->displayCal->day = 1;
    }

    public function displayToday()
    {
        $this->displayCal = clone now();
        $this->displayCal->day = 1;
    }

    public function displayEndValue()
    {
        if (!$this->endValue) {
            return;
        }
        $this->displayCal = clone $this->endValue;
        $this->displayCal->day = 1;
    }

    public function displayStartValue()
    {
        if (!$this->endValue) {
            return;
        }
        $this->displayCal = clone $this->startValue;
        $this->displayCal->day = 1;
    }

    public function setMin($min)
    {
        $this->min = DateTime::createFromFormat("Y-m-d", $min);
    }

    public function setMax($max)
    {
        $this->max = DateTime::createFromFormat("Y-m-d", $max);
    }

    public function showNextMonth()
    {
        $this->displayCal->addMonth(1);
        $this->displayCal->day = 1;
    }

    public function showPrevMonth()
    {
        $this->displayCal->subMonth(1);
        $this->displayCal->day = 1;
    }
}
