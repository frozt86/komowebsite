<?php

namespace App\Livewire;

use DateTime;
use Livewire\Component;
use Livewire\Attributes\On;
use Illuminate\Support\Carbon;

class Datepicker extends Component
{

    public $values = [];
    public $min = null;
    public $max = null;
    public $displayCal = null;
    public $id = null;
    public $multipleMode = false;

    // ***************
    // LIFECYCLE HOOKS
    // ***************
    public function mount(string $max = null, string $min = null, $value = null, $id = null, $multipleMode = false)
    {
        if ($value) {
            $cValue = Carbon::createFromFormat("Y-m-d H:i:s", $value)->setTime(0, 0, 0, 0);
            $this->values = [$cValue];
        }

        $this->displayCal = $value ? $value : now();
        $this->displayCal->day = 1;
        $this->id = $id;
        $this->multipleMode = $multipleMode;
    }

    public function render()
    {
        return view('livewire.datepicker');
    }

    // **************
    // EVENT LISTENER
    // **************

    // ***************
    // OTHER FUNCTIONS
    // ***************
    public function selectDate($date)
    {
        $sDate = Carbon::createFromFormat("Y-m-d H:i:s", "$date 00:00:00");
        if ($this->multipleMode) {
            $nValues = array_reduce($this->values, function ($result, $val) use ($sDate) {
                if ($val->toDateString() === $sDate->toDateString()) {
                    return $result;
                }
                array_push($result, $val);
                return $result;
            }, []);

            if (count($this->values) > count($nValues)) {
                $this->values = $nValues;
            } else {
                array_push($this->values, $sDate);
            }
        } else {
            $this->values = [$sDate];
        }
        $this->displayCal = clone $sDate;
        $this->displayCal->day = 1;
        $this->dispatch('date-selected', id: $this->id, values: $this->values);
    }

    public function displayToday()
    {
        $this->displayCal = now();
        $this->displayCal->day = 1;
    }

    public function displaySelected()
    {
        if ($this->multipleMode) {
            return;
        }

        if (count($this->values) > 0) {
            $this->displayCal = clone $this->values[0];
            $this->displayCal->day = 1;
        };
    }

    public function setMin($min)
    {
        $this->min = DateTime::createFromFormat("Y-m-d", $min);
    }

    public function setMax($max)
    {
        $this->max = DateTime::createFromFormat("Y-m-d", $max);
    }

    public function showNextMonth()
    {
        $this->displayCal->addMonth(1);
        $this->displayCal->day = 1;
    }

    public function showPrevMonth()
    {
        $this->displayCal->subMonth(1);
        $this->displayCal->day = 1;
    }

    public function toggleMultiple()
    {
        $this->multipleMode = !$this->multipleMode;
        if (!$this->multipleMode) {
            if (count($this->values) > 0) {
                asort($this->values);
                $this->values = [current($this->values)];
            }
        }
    }
}
